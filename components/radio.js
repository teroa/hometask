export default ({ selected }) => (
    <div className='outer'>
        <div className='inner'>
        </div>
        <style jsx>{`
            .outer {
                display: inline-block;
                position: relative;
                background: white;
                width: 1.5em;
                height: 1.5em;
                overflow: hidden;
                border-radius: 50%;
                border: 1px solid black;
            }
            .inner {
                position: absolute;
                background: grey;
                border-radius: 50%;
                z-index: 2;
                margin: 15%;
                height: 70%;
                width: 70%;
            }
        `}</style>
    </div>
)