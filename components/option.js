
export default ({ id, title, thumbnail, selected, offsetX, offsetY, rotation, clickHandler }) => {
    const imageStyle = {
        marginLeft: offsetX,
        marginTop: offsetY
    }
    const containerStyle = {}
    if (selected) {
        imageStyle.WebkitFilter = 'grayscale(0)'
        imageStyle.filter = 'grayscale(0)'
        imageStyle.WebKitTransform = 'rotate(' + rotation + ')'
        imageStyle.transform = 'rotate(' + rotation + ')'
        containerStyle.borderColor = '#66F'
    }
    return (
        <div className="options-container" onClick={() => clickHandler(id)} style={containerStyle}>
            <img className="image" src={thumbnail} alt={title} style={imageStyle} />
            <p className="title">{title}</p>
            <style jsx>{`
            .options-container {
                position: relative;
                box-sizing: content-box;
                width: 10em;
                height: 10em;
                border: 0.4em solid #666;
                overflow: hidden;
                border-radius: 50%;
                margin: 0.5em;
            }
            .image {
                position: absolute;
                width: 130%;
                vertical-align: top;
                z-index: 1;
                -webkit-filter: grayscale(0.5);
                filter: grayscale(0.5);
                transition: transform .5s ease-in-out
            }
            .title {
                position: absolute;
                font-size: 1.1em;
                z-index: 2;
                padding-bottom: 0.3em;
                line-height: 1.6;
                bottom: 0;
                width: 100%;
                margin: 0;
                text-align: center;
                color: #fff;
                text-shadow: 1px 1px 2px #000;
                background: rgba(0, 0, 0, 0.2);
            }

        `}</style>
        </div>
    )
}