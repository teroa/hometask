import Link from 'next/link'

export default (props) => {
  const { href, disabled, children } = props
  const disabledStyle = disabled ? {
    color: '#ccc',
    background: '#666'
  } : null
  return (
    <span className="button-outer" style={disabled ? { borderColor: '#666' } : {}}>
      {href
      ?
        <Link href={href} prefetch>
          <a>
            <span className={'link-button'} style={disabledStyle}>
              {children}
            </span>
          </a>
        </Link>
      :
          <span className={'link-button link-button--disabled'} style={disabledStyle}>
            {children}
          </span>
      }
      <style jsx>{`
        .button-outer {
          display: inline-block;
          border-radius: 0.45em;
          overflow:hidden;
          border: 2px solid #66F;
          margin: 2em;
        }
        .link-button {
          display: inline-block;
          font-size: 1.2em;
          font-weight: bolder;
          color: #fff;
          border-radius: 0.4em;
          border: 1px solid #fff;
          display: inline-block;
          padding: 0.5em 1em;
          background: #66F;
        }
        .link-button--disabled {
          color: #ccc;
          background: #666;
        }
        .link-button:active {
          color: #fff;
        }
    `}</style>
    </span>
  )
}