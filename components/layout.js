import Head from 'next/head'

export default ({ children, title = '--' }) => (
  <div>
    <Head>
      <title>{ title }</title>
      <meta charSet='utf-8' />
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
    </Head>

    <div className="content">

    { children }

    </div>

    <footer>
      <p>Tero Aarnio</p>
    </footer>

    <style jsx global>{`
      html {
        font: 12px menlo;
        height: 100%;
      }
      body {
        background: #fff;
        font: 12px menlo;
        color: #000;
        margin: 0;
        padding-bottom: 2em;
      }
      .content {
        margin: 1em;
      }
      footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: #666;
        color: white;
        text-align: center;
        line-height: 2em;
      }
    `}</style>
  </div>
)