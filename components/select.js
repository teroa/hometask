import React from 'react'
import PropTypes from 'prop-types'
import Option from './option'

export default class extends React.PureComponent {
    state = {
        selection: ''
    }

    static proptypes = {
        options: PropTypes.shape({
            id: PropTypes.string,
            title: PropTypes.string,
            thumbnail: PropTypes.string,
            offsetX: PropTypes.string,
            offsetY: PropTypes.string,
            rotation: PropTypes.string
        }).isRequired,
        selectedHandler: PropTypes.func.isRequired
    }

    optionCLickHandler = (id) => {
        this.props.selectedHandler(id)
        this.setState({
            selection: id
        })
    }

    render() {
        const options = Array.isArray(this.props.options) ? this.props.options : []
        return (
            <div className="select-container">
                {this.props.options.map(
                    ({ id, title, thumbnail, offsetX, offsetY, rotation }) => (
                        <Option key={id} id={id} title={title}
                            thumbnail={thumbnail}
                            offsetX={offsetX} offsetY={offsetY}
                            rotation={rotation}
                            selected={this.state.selection === id}
                            clickHandler={this.optionCLickHandler}
                        />
                    )
                )}
                <style jsx>{`
                    .select-container {
                        display: flex;
                    }
                `}</style>
            </div>
        )
    }
}