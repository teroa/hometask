const ANIMALS = [
    {
        id: 'kissa',
        title: 'Kissa',
        thumbnail: 'https://upload.wikimedia.org/wikipedia/commons/b/b2/WhiteCat.jpg',
        offsetY: '-10%',
        offsetX: '-15%',
        rotation: '10deg',
        url: 'https://fi.wikipedia.org/w/api.php?action=query&prop=extracts|images&exintro&explaintext&format=json&formatversion=2&titles=Kissa'
    },
    {
        id: 'koira',
        title: 'Koira',
        thumbnail: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Finnish_Spitz_600.jpg/500px-Finnish_Spitz_600.jpg',
        offsetY: '-5%',
        offsetX: '-20%',
        rotation: '-10deg',
        url: 'https://fi.wikipedia.org/w/api.php?action=query&prop=extracts|images&exintro&explaintext&format=json&formatversion=2&titles=Koira'
    },
    {
        id: 'kirahvi',
        title: 'Kirahvi',
        thumbnail: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Giraffe-closeup-head.jpg/320px-Giraffe-closeup-head.jpg',
        offsetY: '-30%',
        offsetX: '-10%',
        rotation: '-10deg',
        url: 'https://fi.wikipedia.org/w/api.php?action=query&prop=extracts|images&exintro&explaintext&format=json&formatversion=2&titles=Kirahvi'
    }
]

export const getOptions = () => {
    return ANIMALS.map(({id, title, thumbnail, offsetY, offsetX, rotation}) => (
        {id, title, thumbnail, offsetY, offsetX, rotation})
    )
}

export const getAnimalById = (id) => {
    const animal = ANIMALS.find((animal) => (animal.id === id))
    //console.log('id animal ANIMALS', '"' +id+'"', animal, ANIMALS)
    if (animal) {
        return {...animal}
    }
    return null
}

export const getURLs = () => {
    return ANIMALS.map(({id, URL}) => ({[id]: URL}))
}