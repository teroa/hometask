import React from 'react'
import Router from 'next/router'

import Layout from '../components/layout'
import Select from '../components/select'
import Button from '../components/button'
import Link from 'next/link'
import { getOptions } from '../model/animals'

const animalOptions = getOptions()

export default class extends React.Component {
    state = {
        selectedId: ''
    }

    selectedHandler = (id) => {
        this.setState({
            selectedId: id
        })
    }

    render() {
        const selectedId = this.state.selectedId

        return <Layout title="Valitse suosikkieläimesi">
            <div className="home-container">
                <h1>Valitse oma suosikkisi:</h1>
                <Select options={animalOptions} selectedHandler={this.selectedHandler} />

                <Button
                    href={selectedId ? '/selection/' + selectedId : null}
                    disabled={!selectedId}>
                        Lähetä
                </Button>
            </div>
            <style jsx>{`
            .home-container {
                display: flex;
                flex-direction: column;
                align-items: center;
            }
            `}</style>
        </Layout>
    }
}