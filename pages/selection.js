import React from 'react'
import Link from 'next/link'
import Router from 'next/router'

import Layout from '../components/layout'
import { getAnimalById } from '../model/animals'
import Button from '../components/button'

async function fetchAnimalDataOnServer(id, fetchFunc) {
    //console.log('fetchAnimalDataOnServer ' + id)
    const animal = getAnimalById(id)
    if (!animal) {
        return {}
    }
    const nodeFetch = require('node-fetch')

    try {
        const response = await nodeFetch(animal.url, {
            headers: {
                'User-Agent': 'Hometask/1.0 (https://www.steelfinger/; steelfinger@steelfinger.fi)'
            }
        })

        const json = await response.json()
        //console.log('json' + JSON.stringify(json, null, 2))
        const page = json.query.pages[0]
        const description = page.extract
        const pageImages = page.images

        const images = await fetchImages(pageImages)

        return {
            animal,
            description,
            images
        }

    } catch (e) {
        console.log(`Fetcing ${animal.url} -> error:`, e)
    }
    return {}
}

async function fetchImages(pageImages) {
    // example url https://fi.wikipedia.org/w/api.php?action=query&titles=Tiedosto%3AGiraffe-closeup-head.jpg&prop=imageinfo&iiprop=url&format=json
    const imageInfoURL1 = 'https://fi.wikipedia.org/w/api.php?action=query&titles='
    const imageInfoURL2 = '&prop=imageinfo&iiprop=url&format=json'
    const nodeFetch = require('node-fetch')
    const maxImages = 10
    let images = []
    for (const image of pageImages) {
        if (images.length >= maxImages) {
            break;
        }
        if (image.title.match(/.jpg$/gi) !== null) {
            const infoURL = imageInfoURL1 + encodeURIComponent(image.title) + imageInfoURL2
            //console.log('--fetching image ' + image.title, infoURL)
            try {
                const response = await nodeFetch(infoURL, {
                    headers: {
                        'User-Agent': 'Hometask/1.0 (https://www.steelfinger/; steelfinger@steelfinger.fi)'
                    }
                })
                let json = await response.json()
                //console.log('--fetched image ' + image.title, JSON.stringify(json.query, null, 2))
                json = JSON.parse(JSON.stringify(json.query))
                let imageURL = json.pages['-1'].imageinfo[0].url
                //console.log('--add image ' + imageURL)
                images.push(imageURL)
            } catch (error) {
                console.log("!!!ERROR fetching image data".erro)
            }
        }
    }
    return images
}

export default class extends React.Component {

    static async getInitialProps({ query }) {
        //console.log('getInitialProps ' + query.id)
        const id = query && query.id
        if (!id) {
            return {}
        }
        return await fetchAnimalDataOnServer(id)
    }

    renderImages() {
        this.props.imageURLs.map((url) => {
            <div className="image">
                <img src={url} alt="" />
            </div>
        })
    }

    render() {

        const animal = this.props.animal

        if (!animal) {
            return <p>Ladataan...</p>
        }

        const description = this.props.description || '--'

        const wikipediaURL = 'https://fi.wikipedia.org/wiki/' + animal.title

        return (
            <Layout title={'Valitsit suosikkieläimesi ' + animal.title}>
                <h1>Suosikkisi on {animal.title.toLowerCase()}</h1>
                <h2>Erinomainen valinta!</h2>
                <h3>Näin wikipedia kertoo eläimestä:</h3>
                <p className="description">{description}</p>
                <p><a href={wikipediaURL} target="_blank">{wikipediaURL.substr(8)}</a></p>
                <div className="image-container">
                    {
                        this.props.images.map((url, index) => (
                            <div className="image" key={'_' + index}>
                                <img src={url} alt="" />
                            </div>
                        ))
                    }
                </div>
                <div>
                    <Button href={'/'} disabled={false}>
                        Valitse toinen eläin
                    </Button>
                </div>
                <style jsx>{`
                    .description {
                        font-size: 1.2em;
                        line-height: 1.3;
                    }
                    .image-container {
                        display: flex;
                        flex-wrap: wrap;
                        align-items: center;
                    }
                    .image {
                        display: inline-block;
                        align-self: center;
                        width: 240px;
                        max-height: 320px;
                        height: auto;
                        overflow: hidden;
                        padding: 0.5em;
                    }
                    .image > img {
                        width: 100%;
                    }
                `}</style>
            </Layout>
        )
    }
}